<?php 

	$data = [32, 13, 312, 21, 21, 32, 12, 42, 13, 10, 234, 12];

?>


<!DOCTYPE html>
<html>
<head>
	<title></title>

	<script src="node_modules/chart.js/dist/Chart.min.js"></script>
	<link rel="stylesheet" href="node_modules/chart.js/dist/Chart.min.css"/>
</head>
<body style="background-color: beige;">


	<canvas id="myChart" width="20" height="10"></canvas>

	<script>
	var ctx = document.getElementById('myChart');
	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange', 'Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
	        datasets: [{
	            label: '# of Votes',
	            data: [<?php 
	            			foreach($data as $value){
	            				echo $value.",";
	            			} 
	            		?>
	            ],
	            backgroundColor: [
	                'rgba(255, 99, 132, 0.2)',
	                'rgba(54, 162, 235, 0.2)',
	                'rgba(255, 206, 86, 0.2)',
	                'rgba(75, 192, 192, 0.2)',
	                'rgba(153, 102, 255, 0.2)',
	                'rgba(255, 159, 64, 0.2)',
	                'rgba(255, 159, 64, 0.2)',
	                'rgba(255, 159, 64, 0.2)',
	                'rgba(255, 159, 64, 0.2)',
	                'rgba(255, 159, 64, 0.2)',
	                'rgba(255, 159, 64, 0.2)',
	                'rgba(255, 159, 64, 0.2)'
	            ],
	            borderColor: [
	                'rgba(255, 99, 132, 1)',
	                'rgba(54, 162, 235, 1)',
	                'rgba(255, 206, 86, 1)',
	                'rgba(75, 192, 192, 1)',
	                'rgba(153, 102, 255, 1)',
	                'rgba(255, 159, 64, 1)',
	                'rgba(255, 159, 64, 1)',
	                'rgba(255, 159, 64, 1)',
	                'rgba(255, 159, 64, 1)',
	                'rgba(255, 159, 64, 1)',
	                'rgba(255, 159, 64, 1)',
	                'rgba(255, 159, 64, 1)',
	                'rgba(255, 159, 64, 1)'
	            ],
	            borderWidth: 1
	        }]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero: true
	                }
	            }]
	        }
	    }
	});
	</script>


</body>
</html>